package com.weather.android.application.ui;

/**
 * Created by Rajesh on 08-01-2017.
 */
public interface UIListener<T> {

    void onSuccess(T t);

    void onFailure(String errorCode);

}
