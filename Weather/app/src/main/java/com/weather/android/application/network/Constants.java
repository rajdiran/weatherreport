package com.weather.android.application.network;

/**
 * Created by Rajesh on 08-01-2017.
 */
public class Constants {

    public static String ERR_UNKNOWN = "ERR_300";

    public static String ERR_NO_INTERNET = "ERR_800";

    public static String URL_WEATHER_REPORT = "http://api.openweathermap.org/data/2.5/weather?q=London&APPID=97379a6635c2d1da26a58fc6eede8637";

}
