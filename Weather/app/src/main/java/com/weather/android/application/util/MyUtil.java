package com.weather.android.application.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Rajesh on 08-01-2017.
 */
public class MyUtil {

    private static final String FORMAT_DATE = "dd-MM-yyyy HH:mm";

    public static boolean isInternetAvailable(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE); // 1
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo(); // 2
        return networkInfo != null && networkInfo.isConnected();
    }

    public static String getTemperature(JSONObject jsonObject) {

        if (null == jsonObject)
            return "";

        try {

            if(jsonObject.has("main")){
                JSONObject tempJsonObj = jsonObject.getJSONObject("main");
                if(tempJsonObj.has("temp")){
                    return BigDecimal.valueOf(tempJsonObj.getDouble("temp")).toPlainString();
                }
            }

        } catch (JSONException e) {
                    }
        return "";
    }

    public static String convertLongToString(long milliseconds){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliseconds);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(FORMAT_DATE);
        return simpleDateFormat.format(calendar.getTime());
    }

    public static String getCurrentTime(){
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(FORMAT_DATE);
        return simpleDateFormat.format(calendar.getTime());
    }

}
