package com.weather.android.application.ui.activity;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.weather.android.application.R;
import com.weather.android.application.db.helper.MyDatabaseHelper;
import com.weather.android.application.model.Temperature;
import com.weather.android.application.ui.adapter.HistoryListAdapter;

import java.util.List;

public class WeatherHistory extends AppCompatActivity {

    private ProgressBar mProgressBar;

    private RecyclerView mHistoryListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_history);

        initViews();

        new HistoryLoader().execute();

    }

    private void initViews() {

        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);

        mHistoryListView = (RecyclerView) findViewById(R.id.history_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mHistoryListView.setLayoutManager(mLayoutManager);
        mHistoryListView.setItemAnimator(new DefaultItemAnimator());

    }

    private class HistoryLoader extends AsyncTask<Void, Void, List<Temperature>>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mHistoryListView.setVisibility(View.GONE);
            mProgressBar.setVisibility(View.VISIBLE);

        }

        @Override
        protected List<Temperature> doInBackground(Void... voids) {

            MyDatabaseHelper myDatabaseHelper = new MyDatabaseHelper(WeatherHistory.this);
            return myDatabaseHelper.getTemperatues();

        }

        @Override
        protected void onPostExecute(List<Temperature> temperatures) {
            super.onPostExecute(temperatures);

            if(isFinishing())
                return;

            mHistoryListView.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(View.GONE);

            if(null == temperatures || temperatures.isEmpty()){
                Toast.makeText(WeatherHistory.this, "No History!", Toast.LENGTH_SHORT).show();
                return;
            }

            mHistoryListView.setAdapter(new HistoryListAdapter(WeatherHistory.this, temperatures));

        }
    }

}
