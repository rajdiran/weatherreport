package com.weather.android.application.db.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.weather.android.application.model.Temperature;
import com.weather.android.application.util.MyUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rajesh on 08-01-2017.
 */
public class MyDatabaseHelper extends SQLiteOpenHelper{

    public static final String DATABASE_NAME = "weather.db";
    private static final int DATABASE_VERSION = 1;

    public static final String WEATHER_TABLE_NAME = "weather";
    public static final String WEATHER_COLUMN_ID = "_id";
    public static final String WEATHER_COLUMN_TEMPERATURE = "temperature";
    public static final String WEATHER_COLUMN_TIME = "time"; // time in milliseconds


    public MyDatabaseHelper(Context context) {
        super(context, DATABASE_NAME , null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + WEATHER_TABLE_NAME + "(" +
                WEATHER_COLUMN_ID + " INTEGER PRIMARY KEY, " +
                WEATHER_COLUMN_TEMPERATURE + " TEXT, " +
                WEATHER_COLUMN_TIME + " LONG)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + WEATHER_TABLE_NAME);
        onCreate(db);
    }

    public synchronized void saveTemperature(String temperature){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(WEATHER_COLUMN_TEMPERATURE, temperature);
        contentValues.put(WEATHER_COLUMN_TIME, System.currentTimeMillis());
        db.insert(WEATHER_TABLE_NAME, null, contentValues);
    }

    public synchronized List<Temperature> getTemperatues(){
        SQLiteDatabase db = getReadableDatabase();

        String[] columns = new String[]{ WEATHER_COLUMN_TEMPERATURE, WEATHER_COLUMN_TIME };
        Cursor cursor;
        cursor = db.query(WEATHER_TABLE_NAME, columns, null, null, null, null, WEATHER_COLUMN_TIME+" ASC");

        List<Temperature> temperatureList = new ArrayList<>();

        if(cursor == null)
            return temperatureList;

        int temperatureIndex = cursor.getColumnIndex(WEATHER_COLUMN_TEMPERATURE);
        int timeInMilliSecondsIndex = cursor.getColumnIndex(WEATHER_COLUMN_TIME);

        while (cursor.moveToNext()) {

            Temperature temperature = new Temperature();
            temperature.setTemperature(cursor.getString(temperatureIndex));

            long timeInMillis = cursor.getLong(timeInMilliSecondsIndex);
            temperature.setDateTime(MyUtil.convertLongToString(timeInMillis));

            temperatureList.add(temperature);
        }

        cursor.close();

        return temperatureList;
    }

}
