package com.weather.android.application.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.weather.android.application.R;
import com.weather.android.application.model.Temperature;

import java.util.List;

/**
 * Created by Rajesh on 08-01-2017.
 */
public class HistoryListAdapter extends RecyclerView.Adapter<HistoryListAdapter.MyViewHolder> {

    private List<Temperature> mTemperatureList;

    private Context mContext;

    public HistoryListAdapter(Context context,List<Temperature> temperatureList){
        this.mTemperatureList = temperatureList;
        this.mContext =  context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView temperatureView;

        public MyViewHolder(View view) {
            super(view);
            temperatureView = (TextView) view.findViewById(R.id.temperature_view);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_history, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Temperature temperature = mTemperatureList.get(position);

        String formattedTemperature = String.format(mContext.getString(R.string.row_temperature_format),
                temperature.getDateTime(), temperature.getTemperature());

        holder.temperatureView.setText(formattedTemperature);
    }

    @Override
    public int getItemCount() {
        return mTemperatureList.size();
    }

}
