package com.weather.android.application.model;

/**
 * Created by Rajesh on 08-01-2017.
 */
public class Temperature {

    private String temperature;

    private String dateTime;

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }
}
