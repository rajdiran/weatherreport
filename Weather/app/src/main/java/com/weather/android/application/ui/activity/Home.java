package com.weather.android.application.ui.activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.weather.android.application.R;
import com.weather.android.application.network.Constants;
import com.weather.android.application.serrvice.MyWeatherService;
import com.weather.android.application.ui.UIListener;

import java.util.Calendar;

public class Home extends AppCompatActivity implements View.OnClickListener {

    private Button mHistoryButton;

    private TextView mTemperatureTextView;

    private LinearLayout mSuccessViewContainer;

    private ProgressBar mProgressBar;

    private MyWeatherService mWeatherService;

    private boolean mBound = false;

    enum UIState {
        LOADING, FINISHED
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //initialize views
        initViews();

        startServiceForEveryHour();

    }

    private void initViews() {

        mHistoryButton = (Button) findViewById(R.id.btn_history);
        mHistoryButton.setOnClickListener(this);

        mTemperatureTextView = (TextView) findViewById(R.id.temperature);

        mSuccessViewContainer = (LinearLayout) findViewById(R.id.successView);

        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_history:

                Intent weatherHistoryIntent = new Intent(Home.this, WeatherHistory.class);
                startActivity(weatherHistoryIntent);

                break;
        }

    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent weatherServiceIntent = new Intent(this, MyWeatherService.class);
        bindService(weatherServiceIntent, mServiceConnection, Context.BIND_AUTO_CREATE);

    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mBound) {
            unbindService(mServiceConnection);
            mBound = false;
        }

    }

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {

            MyWeatherService.MyWeatherBinder binder = (MyWeatherService.MyWeatherBinder) iBinder;
            mWeatherService = binder.getService();
            mBound = true;

            mWeatherService.getCurrentWeather(uiListener);
            updateUIState(UIState.LOADING);

        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {

            mBound = false;

        }
    };

    private UIListener<String> uiListener = new UIListener<String>() {

        @Override
        public void onSuccess(String temperature) {
            if (isFinishing())
                return;

            updateUIState(UIState.FINISHED);

            if (!TextUtils.isEmpty(temperature)) {
                mTemperatureTextView.setText(temperature);
            } else {
                Toast.makeText(Home.this, getString(R.string.message_unknown_error), Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        public void onFailure(String errorCode) {

            if (isFinishing())
                return;

            updateUIState(UIState.FINISHED);

            if (Constants.ERR_NO_INTERNET.equalsIgnoreCase(errorCode)) {
                Toast.makeText(Home.this, getString(R.string.message_no_internet), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(Home.this, getString(R.string.message_unknown_error), Toast.LENGTH_SHORT).show();
            }
        }

    };

    private void updateUIState(UIState state){
        switch (state) {
            case LOADING:
                mSuccessViewContainer.setVisibility(View.GONE);
                mProgressBar.setVisibility(View.VISIBLE);
                break;
            case FINISHED:
                mProgressBar.setVisibility(View.GONE);
                mSuccessViewContainer.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void startServiceForEveryHour() {
        Calendar cal = Calendar.getInstance();
        Intent intent = new Intent(this, MyWeatherService.class);
        PendingIntent pIntent = PendingIntent.getService(this, 0, intent, 0);

        AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        // Start service every hour
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),
                1000 * 60 * 60, pIntent);
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }
}
