package com.weather.android.application.serrvice;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;

import com.weather.android.application.ui.activity.Home;
import com.weather.android.application.R;
import com.weather.android.application.db.helper.MyDatabaseHelper;
import com.weather.android.application.network.Constants;
import com.weather.android.application.network.manager.ServiceManager;
import com.weather.android.application.ui.UIListener;
import com.weather.android.application.util.MyUtil;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Rajesh on 08-01-2017.
 */
public class MyWeatherService extends Service {

    private final IBinder mBinder = new MyWeatherBinder();


    public class MyWeatherBinder extends Binder {
        public MyWeatherService getService() {
            return MyWeatherService.this;
        }
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        getCurrentWeather(null);

        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    public void getCurrentWeather(UIListener uiListener) {

        if (!MyUtil.isInternetAvailable(MyWeatherService.this)) {
            uiListener.onFailure(Constants.ERR_NO_INTERNET);
            return;
        }

        new FetchTemperature(uiListener).execute();

    }

    private class FetchTemperature extends AsyncTask<Void, Void, String> {

        UIListener<String> uiListener;

        FetchTemperature(UIListener<String> uiListener) {
            this.uiListener = uiListener;
        }

        boolean successResponse = false;

        @Override
        protected String doInBackground(Void... voids) {

            String response = ServiceManager.getWeatherReport();

            if (!TextUtils.isEmpty(response)) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String temperature = MyUtil.getTemperature(jsonObject);
                    if (!TextUtils.isEmpty(temperature)) {
                        successResponse = true;

                        MyDatabaseHelper dbHelper = new MyDatabaseHelper(getApplicationContext());
                        dbHelper.saveTemperature(temperature);

                        return temperature;
                    }
                    return MyUtil.getTemperature(jsonObject);
                } catch (JSONException e) {
                    return Constants.ERR_UNKNOWN;
                }
            }

            return Constants.ERR_UNKNOWN;

        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);

            if (null == uiListener) {
                if (successResponse)
                    showNotification(response);
                stopSelf();
                return;
            }

            if (successResponse)
                uiListener.onSuccess(response);
            else
                uiListener.onFailure(response);
        }
    }

    private void showNotification(String temperature) {

        String formattedTemperature = String.format(getString(R.string.row_temperature_format),
                MyUtil.getCurrentTime(), temperature);

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle("Temperature!")
                        .setContentText(formattedTemperature);

        Intent notificationIntent = new Intent(this, Home.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);

        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());

    }


}
